# Filename: <MechanicalTest_Aquistion.py>
# Copyright (C) <2021> Authors: <Pierre Vacher, Ludovic Charleux, Emile Roux, Christian Elmo Kulanesan>
# 
# This program is free software: you can redistribute it and / or 
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 2 of the License. 
# 
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of  
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
# GNU General Public License for more details.  
# 
# You should have received a copy of the GNU General Public License  
# along with this program.  If not, see <https://www.gnu.org/licenses/>. 
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 25 16:26:31 2019

@author: rouxemi
"""

import numpy as np 
import pandas as pd 
import matplotlib as mpl
import matplotlib.pyplot as plt
import time
from matplotlib import cm
import cv2
import csv
from pycama import camera
from PyDAQmx import *


# =============================================================================
# Settings
# =============================================================================
# test Name
out_name = "Compression"
# Sample
meta = {}
meta['ep'] = .0
meta['lar'] = 7.20
meta['h'] = .0
meta['diam'] = .0
# test description
meta['Description'] = 'Compression 0°'
meta['test_type']= 'Compression'
meta['Dir'] = 0
meta['Vit'] = -1.

# Aquitision settings
meta['Force_pleine_Echelle'] = -20000 #N
meta['V_pleine_Echelle'] = 10.014
meta['V_Zero'] = 0.02

# snapping images Triger 
dt_entre_img = 5 # seconde
dF_entre_img = 100 # N

# Write it into the csv file
metadf = pd.DataFrame.from_dict(meta, orient =  'index')
metadf.to_csv(out_name+'.csv',index=True)

# =============================================================================
# CAMERA
# =============================================================================
# GenICam file

path2cti = r'C:\Program Files\MATRIX VISION\mvIMPACT Acquire\bin\x64\mvGenTLProducer.cti'
camcap = camera.CameraGenICam(path2cti=path2cti,
                 binningV=1,
                 binningH=1,
                 exposureTime=5000,
                 fps=10)
"""
camcap=camera.Camera()
"""
camcap.stream()


# =============================================================================
# Conversion V2Fore
# =============================================================================
def V2Force(V):
    F=(V-meta['V_Zero']) / meta['V_pleine_Echelle'] * meta['Force_pleine_Echelle']
    return F


# =============================================================================
# NIDAQ
# =============================================================================
analog_input = Task()
read = int32()
nb_points = 100
data = np.zeros((nb_points,), dtype=np.float64)

# DAQmx Configure Code
analog_input.CreateAIVoltageChan(r"Dev1/ai0","",DAQmx_Val_Cfg_Default,-10.0,10.0,DAQmx_Val_Volts,None)
#analog_input.CfgSampClkTiming("",1000.0,DAQmx_Val_Rising,DAQmx_Val_FiniteSamps,10000)
# DAQmx Start Code
analog_input.StartTask()


# =============================================================================
# MAIN LOOP
# =============================================================================
t_aqui = 0
F_aqui = 0
t_0 = time.time()
img_counter=0


# Writer header of the dataframe into the csv file
with open(out_name+'.csv','a', newline='') as fd:
    writer = csv.writer(fd)
    cvs_rows = ['id','t_s','Volatge_V','Force_N', 'img_name']            
    writer.writerow(cvs_rows)

            
with open(out_name+'.csv','a', newline='') as fd:
    writer = csv.writer(fd)
    while True :
        t = time.time()
        
        # Read Force   
        analog_input.ReadAnalogF64(nb_points,10.0,
                                   DAQmx_Val_GroupByChannel,
                                   data,1000,read,None)       
        V = data.mean()
        F = V2Force(V)    
        
      
        if (np.abs(t-t_aqui)>dt_entre_img) | (np.abs(F_aqui-F) > dF_entre_img) :                       
            t_aqui = time.time()
            F_aqui = F
            frame = camcap.get_frame()
            print('Force = {} N'.format( V2Force(V)))             
            img_name = "{0}_{1:04d}.tif".format(out_name,img_counter)
            cv2.imwrite(img_name, frame, [cv2.IMWRITE_PNG_COMPRESSION, 0])
            print("{} written!".format(img_name))
            cvs_rows = [str(img_counter), str(t_aqui-t_0), str(V),str(F), img_name]            
            writer.writerow(cvs_rows)
            img_counter += 1
        
        cv2.imshow('frame',frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            print("Finished")
            break

 
camcap.release()







