# Filename: <Generate_calibration_file_ChessBoard.py>
# Copyright (C) <2021> Authors: <Pierre Vacher, Ludovic Charleux, Emile Roux, Christian Elmo Kulanesan>
#
# This program is free software: you can redistribute it and / or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import glob

import cv2
import matplotlib.pyplot as plt
import numpy as np

import image_datasets as imsets
from pycama import calibration

plt.close('all')
# =============================================================================
#                               SETTINGS
# =============================================================================
print("#SETTING UP DATASET")
datasets_metadata = imsets.core.get_metadata()
dataset_path = [p for p in datasets_metadata.index
                if "/camera/calibration/SVS_EXO542MU3/dataset1" in p][0]
directory, image_pathes = imsets.core.get_image_pathes(
    dataset_path, relative=False)
print(datasets_metadata.loc[dataset_path])


# Specify where you want to save the output .json calibration file and its name
# example name : <.\FOLDER_OUT\DEVICE-NAME_WIDTHxHEIGTH_(facultative-arg).json >

device_name = datasets_metadata.loc[dataset_path].camera_model
w, h = datasets_metadata.loc[dataset_path].resolution.split('x')
WIDTH = int(w)
HEIGTH = int(h)

path = "./{2}_{0}x{1}.json".format(w, h, device_name)
# =============================================================================
#                              PROCESSING
# =============================================================================
# path to Images for calibration
images = [directory + image_pathe for image_pathe in image_pathes]
# performe the calibration
flags = 0
#flags = cv2.CALIB_RATIONAL_MODEL
flags += cv2.CALIB_FIX_ASPECT_RATIO
flags += cv2.CALIB_ZERO_TANGENT_DIST
#flags += cv2.CALIB_FIX_K3

ret, out = calibration.calibrate_camera(
    images=images, device=device_name,
    inside_corners=(18, 12),
    width=WIDTH, heigth=HEIGTH,
    sub_pix=True,
    flags=flags,
    mire_type='ChessBoard')
# save the calib
calibration.save_camera_calibration(path, out['camera_matrix'],
                                    out['distortion_coefficients'])
# Plotting
calibration.plot_calibration_info(out)
calibration.plot_distortion_map(out)
