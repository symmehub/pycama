# Filename: <SimpleUseOf_CameraParamterWidget.py>
# Copyright (C) <2021> Authors: <Pierre Vacher, Ludovic Charleux, Emile Roux, Christian Elmo Kulanesan>
# 
# This program is free software: you can redistribute it and / or 
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 2 of the License. 
# 
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of  
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
# GNU General Public License for more details.  
# 
# You should have received a copy of the GNU General Public License  
# along with this program.  If not, see <https://www.gnu.org/licenses/>. 
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 16 10:07:57 2020

@author: rouxemi
"""
import matplotlib.pyplot as plt
from pycama import camera
import cv2
import os
import inspect
import pycama
import json
import pycopo as pcp
import numpy as np
from cv2 import aruco
from pycama import camera_GUI
pycama_path = os.path.dirname(inspect.getfile(pycama)) + "/../"

# =============================================================================
# Camera Settings
# =============================================================================
cap = camera.CameraOpencv(id_cam=1,
                          exposure=2,
                          focus=25)
cap.cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920.0)
cap.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080.0)

# =============================================================================
# Camera GUI
# =============================================================================

ex = camera_GUI.CameraParamterWidget()
ex.show()
ex.update(camera=cap)

# =============================================================================
# Stream Video Flux
# =============================================================================
cap.stream()
cap.release()
