# Filename: <license_gnu.py>
# Copyright (C) <2021> Authors: <Pierre Vacher, Ludovic Charleux, Emile Roux, Christian Elmo Kulanesan>
# 
# This program is free software: you can redistribute it and / or 
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 2 of the License. 
# 
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of  
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
# GNU General Public License for more details.  
# 
# You should have received a copy of the GNU General Public License  
# along with this program.  If not, see <https://www.gnu.org/licenses/>. 
import os
import sys
from datetime import date

dir = "../"

authors = "Pierre Vacher, Ludovic Charleux, Emile Roux, Christian Elmo Kulanesan"
year = date.today().strftime("%Y")
GNU = "# Copyright (C) <" + year + "> Authors: <" + authors + ">\r\n" + \
    "# \r\n" + \
    "# This program is free software: you can redistribute it and / or" + " \r\n" + \
    "# modify it under the terms of the GNU General Public License as published" + " \r\n" + \
    "# by the Free Software Foundation, either version 2 of the License." + " \r\n" + \
    "# \r\n" + \
    "# This program is distributed in the hope that it will be useful," + " \r\n" + \
    "# but WITHOUT ANY WARRANTY; without even the implied warranty of " + " \r\n" + \
    "# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the " + " \r\n" + \
    "# GNU General Public License for more details. " + " \r\n" + \
    "# \r\n" + \
    "# You should have received a copy of the GNU General Public License " + " \r\n" + \
    "# along with this program.  If not, see <https://www.gnu.org/licenses/>. "

# construct full file path recursively
file_list = []
for root, d_names, f_names in os.walk(dir):
    for f in f_names:
        if f.endswith(".py"):
            file_list.append(os.path.join(root, f))

for pyscript in file_list:
    with open(pyscript, 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write("# Filename: <" + pyscript.split("/")[-1] + ">"
                '\n' +
                GNU +
                '\n' +
                content)
