# Filename: <calibration.py>
# Copyright (C) <2021> Authors: <Pierre Vacher, Ludovic Charleux, Emile Roux, Christian Elmo Kulanesan>
# 
# This program is free software: you can redistribute it and / or 
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 2 of the License. 
# 
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of  
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
# GNU General Public License for more details.  
# 
# You should have received a copy of the GNU General Public License  
# along with this program.  If not, see <https://www.gnu.org/licenses/>. 
# ############################################################################
# PYCAMA/CALIBRATION
# ############################################################################


import cv2
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import pandas as pd
import json
from datetime import datetime
from cv2 import aruco


# ####################################
# CAMERA CALIBRATION FUNCTION
# ####################################
def calibrate_camera(device, width, heigth, images,
                     inside_corners=(9, 6),
                     cameraMatrix=None,
                     distCoeffs=None,
                     plot=True,
                     flags=0,
                     sub_pix=True,
                     mire_type='ChessBoard',
                     aruco_dict=aruco.Dictionary_get(aruco.DICT_6X6_250)):
    """
    Camera calibration function
    returns a CameraCalibration instance and the global reprojection error
    inputs :
        -device: name of the device
        -width, heigth: images size
        -images: (liste)  pathes of images to analyse
        -inside_corners: (tuple) defining the number of inside corners
            of the observbed chessboard or aruco board
        -cameraMatrix: camera matirx, if not none used for initilisation
        -distCoeffs: dist coef vector, if not none used for initilisation
        -plot: (boolean) to active plots
        -flags: cv2 flags to set the camera modele to calibrate (
                cv2.CALIB_RATIONAL_MODEL,
                cv2.CALIB_FIX_ASPECT_RATIO,
                cv2.CALIB_ZERO_TANGENT_DIST )
        -sub_pix: subpix refinement flag for corner detection
        -mire_type : ChessBoard or ArucoBoard
        -aruco_dict : aruco dictionary used on the arcuo board

    """
    # dict for output data
    out = {}
    calib_img = []

    # data frame initilisation
    col_names = ['img', 'x_obj', 'y_obj', 'x',
                 'y', 'rep_x', 'rep_y', 'norm', 'file']
    df_corners = pd.DataFrame(columns=col_names)

    img = cv2.imread(images[0])
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    imsize = gray.shape

    objpoints, imgpoints, valid_img = _detect_corners(
        images, inside_corners=inside_corners,
        plot=plot,
        sub_pix=sub_pix,
        mire_type=mire_type,
        aruco_dict=aruco_dict)
    # flags = 0
    # flags = cv2.CALIB_RATIONAL_MODEL
    # flags += cv2.CALIB_FIX_ASPECT_RATIO
    # flags += cv2.CALIB_ZERO_TANGENT_DIST
    # flags += cv2.CALIB_FIX_K3

    criteria = (
        cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER,
        10000,
        10e-8)

    r, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints,
                                                     imgpoints,
                                                     cameraMatrix=cameraMatrix,
                                                     distCoeffs=distCoeffs,
                                                     imageSize=imsize,
                                                     criteria=criteria,
                                                     flags=flags)

    print("camera parametres:")
    print("Camera matrix=", mtx)
    print("dist. coef vector=", dist)
    print("Global reprojection error=", r)

    # point to point reprojection error
    for i in range(len(imgpoints)):
        imgpoints_reproj, _ = cv2.projectPoints(
            objpoints[i], rvecs[i], tvecs[i], mtx, dist)
        r = imgpoints_reproj - imgpoints[i]
        r = r[:, 0, :]
        print(
            "=> Image '{0}', err = {1:4.3f}.".format(
                valid_img[i], np.linalg.norm(
                    r, axis=1).mean()))

        # fullfill data frame
        pos_init = imgpoints[i][:, 0, :]
        objpoints_ = objpoints[i][:, 0:2]
        err = np.linalg.norm(r, axis=1)
        img = cv2.imread(valid_img[i])
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        calib_img.append(gray)
        for j in range(len(pos_init)):
            df_corners.loc[len(df_corners)] = [i,
                                               objpoints_[j, 0],
                                               objpoints_[j, 1],
                                               pos_init[j, 0],
                                               pos_init[j, 1],
                                               r[j, 0],
                                               r[j, 1],
                                               err[j],
                                               valid_img[i]]
    out['camera_matrix'] = mtx
    out['distortion_coefficients'] = dist
    out['device'] = device
    out['width'] = width
    out['heigth'] = heigth

    out['calib_img'] = calib_img
    out['calib_img_path'] = valid_img

    out['chessB_rvecs'] = rvecs
    out['chessB_tvecs'] = tvecs

    out['corners_df'] = df_corners

    out['Date_of_calib'] = datetime.now()

    return r, out


def _detect_corners(images,
                    inside_corners=(9, 6),
                    plot=True,
                    sub_pix=True,
                    mire_type=None,
                    aruco_dict=None):
    """
    Function to detecte corners of a chessboard in images
    inputs:
        - images : (liste)  pathes of images to analyse
        - inside_corners : (tuple) defining the number of inside corners
            of the observbed chessboard
        - plot : (boolean) to active plots

    """

    # definition of chessboard corners in object frame
    nx, ny = inside_corners
    objp = np.zeros((nx * ny, 3), np.float32)
    objp[:, :2] = np.mgrid[0:nx, 0:ny].T.reshape(-1, 2)

    # list initialisation
    objpoints = []  # 3d point in obj space
    imgpoints = []  # 2d points in image plane.
    valid_img = []

    # Stoping criteria for opencv calibration function
    criteria = (
        cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER,
        10000000,
        10e-9)

    # loop over images
    for fname in images:
        img = cv2.imread(fname)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Find the chess board corners
        flags = cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE
        if mire_type == 'ChessBoard':
            ret, corners = cv2.findChessboardCorners(gray,
                                                     (nx, ny),
                                                     flags=flags)
        if mire_type == 'ArucoBoard':
            ret, corners, objp = findArucoBoardCorners(gray,
                                                       (nx, ny),
                                                       aruco_dict)
            objp = objp[:, 0, :]
        # If found, add object points, image points (after refining them)
        if ret:
            if sub_pix:
                # adaptative box size for subpixel detection (it compute the
                # avarage chessBoord size)
                dist = corners[:-1, :] - corners[1:, :]
                dist = np.linalg.norm(dist, axis=1)
                dist = dist[np.where(dist < 2 * dist.mean())]
                ChessBSiez = dist.mean()
                BoxSizeForSubPix = int(np.floor(ChessBSiez / 3.))
                if BoxSizeForSubPix % 2 == 0:
                    BoxSizeForSubPix += 1
                # BoxSizeForSubPix = 11
                # corner detection refininement
                cv2.cornerSubPix(gray,
                                 corners,
                                 (BoxSizeForSubPix, BoxSizeForSubPix),
                                 (-1, -1),
                                 criteria)
            else:
                BoxSizeForSubPix = 0

            # add data to liste
            imgpoints.append(corners)
            valid_img.append(fname)
            objpoints.append(objp)

            print(
                "=> Image '{0}' is valid, BoxSize = {1:d}.".format(
                    fname, BoxSizeForSubPix))
            if plot:
                # Draw and display the corners
                cv2.drawChessboardCorners(img, (nx, ny), corners, ret)
                cv2.imshow('img', img)
                cv2.waitKey(50)
        else:
            print("=> Image '{0}' is rejected.".format(fname))

    return objpoints, imgpoints, valid_img


def findArucoBoardCorners(gray, board_corners, aruco_dict, flags=None):
    ret = False
    corners2d = []
    corners3d = []

    board = aruco.CharucoBoard_create(board_corners[0],
                                      board_corners[1],
                                      1, .75,
                                      aruco_dict)
    # Stoping criteria for opencv calibration function
    criteria = (
        cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER,
        10000000,
        10e-9)
    corners2d, ids, rejectedImgPoints = cv2.aruco.detectMarkers(gray,
                                                                aruco_dict)

    if len(corners2d) > 0:
        ret = True
        # SUB PIXEL DETECTION
        for corner in corners2d:
            cv2.cornerSubPix(gray, corner,
                             winSize=(5, 5),
                             zeroZone=(-1, -1),
                             criteria=criteria)
        retval, corners2d, charucoIds = aruco.interpolateCornersCharuco(
            corners2d,
            ids,
            gray,
            board)
        corners3d = board.chessboardCorners[charucoIds]
    return ret, corners2d, corners3d


def save_camera_calibration(path, camera_matrix, distortion_coefficients):
    """
    Saves a JSON file containing a camera calibration file.

    Inputs:
    * path: path to a JSON file.
    * camera_matrix: a 3x3 float numpy.array instance.
    * distortion_coefficients: a 5 float numpy.array instance.

    Ouputs: None
    """
    C = np.array(camera_matrix).tolist()
    D = np.array(distortion_coefficients).tolist()
    data = {"camera_matrix": C, "distortion_coefficients": D}
    with open(path, "w") as f:
        json.dump(data, f)

# ####################################
# CameraCalibration instance creation
# ####################################


def camera_calibration_from_mtx_dist(mtx, dist, device, width, heigth):
    """
    Creates a CameraCalibration instance from a camera matrix and a distortion
    coefficient matrix.
    """
    dist = dist.flatten()
    k = np.zeros(6)
    k[0] = dist[0]
    k[1] = dist[1]
    if len(dist) >= 5:
        k[2] = dist[4]
    if len(dist) >= 6:
        k[3] = dist[5]
    if len(dist) >= 7:
        k[4] = dist[6]
    if len(dist) >= 8:
        k[5] = dist[7]
    return CameraCalibration(device=device,
                             width=width,
                             heigth=heigth,
                             fx=mtx[0, 0],
                             fy=mtx[1, 1],
                             cx=mtx[0, 2],
                             cy=mtx[1, 2],
                             k1=k[0],
                             k2=k[1],
                             k3=k[2],
                             k4=k[3],
                             k5=k[4],
                             k6=k[5],
                             p1=dist[2],
                             p2=dist[3], )


def read_camera_matrix_from_csv(path):
    """
    Creates a CameraCalibration instance from CSV file.
    inputs :
        path : path of the cvs file containing the camera parameters
    output:
        a CameraCalibration instance
    """
    df = pd.read_csv(path, header=None, index_col=0)

    return CameraCalibration(**df.iloc[:, 0].to_dict())


def plot_calibration_info(out):
    """
    Plot multiple graphs to check calibration
    """
    # plot results
    corners_df = out['corners_df']
    # Gloabl data
    plt.figure()
    plt.hist(corners_df.rep_x, bins=100, alpha=.7, label='err reproj x')
    plt.hist(corners_df.rep_y, bins=100, alpha=.5, label='err reproj y')
    plt.xlabel('Pix')
    plt.ylabel('Count')
    plt.legend()
    plt.title('Reprojection error distribution')
    plt.grid()
    plt.show()
    # Cloud of points
    plt.figure()
    an = np.linspace(0, 2 * np.pi, 100)
    plt.fill(1 * np.cos(an), 1 * np.sin(an), 'g',
             alpha=0.15, label='One pix circle')
    plt.fill(.5 * np.cos(an), 0.5 * np.sin(an), 'p',
             alpha=0.15, label='1/2 pix circle')
    plt.scatter(corners_df.rep_x, corners_df.rep_y, s=2,
                cmap=mpl.cm.Reds,
                label='reprojected points - detected points')
    plt.grid()
    plt.axis('equal')
    plt.title('Reprojection error')
    plt.legend()
    plt.show()

    # Corners by corners
    df_obf = corners_df.groupby(['x_obj', 'y_obj']).rep_y.mean()
    err_grid_y = df_obf.unstack().values.T

    df_obf = corners_df.groupby(['x_obj', 'y_obj']).rep_x.mean()
    err_grid_x = df_obf.unstack().values.T

    plt.figure()
    plt.subplot(1, 2, 1)
    plt.imshow(err_grid_x)
    plt.title('mean Reprojection error corner by corner - x dir.')
    plt.colorbar()
    plt.subplot(1, 2, 2)
    plt.imshow(err_grid_y)
    plt.colorbar()
    plt.title('mean Reprojection error corner by corner - y dir.')
    plt.show()

    # img by img hist
    plt.figure()
    plt.subplot(1, 2, 1)
    df = corners_df.groupby('img').norm.mean() > 0.0
    bad_img = df[df].index

    for i in bad_img:
        plt.hist(corners_df[corners_df.img == i].norm,
                 bins=20, alpha=.5,
                 label='{}'.format(out['calib_img_path'][i]))
    plt.legend()

    plt.subplot(1, 2, 2)
    an = np.linspace(0, 2 * np.pi, 100)
    plt.fill(1 * np.cos(an), 1 * np.sin(an), 'g', alpha=0.15)
    for i in bad_img:
        sub_df = corners_df[corners_df.img == i]
        plt.scatter(sub_df.rep_x, sub_df.rep_y,
                    s=2, cmap=mpl.cm.Reds,
                    label='{}'.format(out['calib_img_path'][i]))
    plt.legend()
    plt.grid()
    plt.axis('equal')
    plt.show()


def plot_distortion_map(out):
    mtx = out['camera_matrix']
    dist = out['distortion_coefficients']
    WIDTH = out['width']
    HEIGTH = out['heigth']
    nx = int(WIDTH / 1)
    ny = int(HEIGTH / 1)
    x = (np.linspace(0, WIDTH, nx) - mtx[0, 2]) / mtx[0, 0]
    y = (np.linspace(0, HEIGTH, ny) - mtx[1, 2]) / mtx[1, 1]
    dist = dist[0]
    x, y = np.meshgrid(x, y)
    r = np.sqrt(x**2 + y**2)

    def distor(x, y, r, dist):
        # ref https://docs.opencv.org/3.4/d9/d0c/group__calib3d.html
        k1 = dist[0]
        k2 = dist[1]
        p1 = dist[2]
        p2 = dist[3]
        k3 = dist[4]
        corr = (1 + k1 * r**2 + k2 * r**4 + k3 * r**6)
        # Rational model
        if dist.size > 5:
            k4 = dist[5]
            k5 = dist[6]
            k6 = dist[7]
            corr /= (1 + k4 * r**2 + k5 * r**4 + k6 * r**6)
        # Distor coordinates
        xd = x * corr + 2 * p1 * x * y + p2 * (r**2 + 2 * x**2)
        yd = y * corr + p1 * (r**2 + 2 * y**2) + 2 * p2 * x * y

        return xd, yd

    # distored coordinates
    xd, yd = distor(x, y, r, dist)

    # back to image scale
    x_img = x * mtx[0, 0] + mtx[0, 2]
    y_img = y * mtx[1, 1] + mtx[1, 2]

    xd_img = xd * mtx[0, 0] + mtx[0, 2]
    yd_img = yd * mtx[1, 1] + mtx[1, 2]

    # translation
    tx = x_img - xd_img
    ty = y_img - yd_img
    t_norm = np.sqrt(tx**2 + ty**2)

    plt.figure()
    plt.subplot(2, 2, 1)
    plt.imshow(tx)
    plt.contour(tx, colors='k')
    plt.axis('off')
    plt.title('x correction [pix]')

    plt.subplot(2, 2, 2)
    plt.imshow(ty)
    plt.contour(ty, colors='k')
    plt.axis('off')
    plt.title('y correction [pix]')

    plt.subplot(2, 2, (3, 4))
    p = plt.imshow(t_norm)
    plt.contour(t_norm, colors='k')
    plt.title('norm of correction [pix]')
    plt.axis('off')
    plt.colorbar(p)


# ####################################
# CameraCalibration Class definition
# ####################################
class CameraCalibration:
    """
    A class to manage camera calibration data.
    """

    def __init__(self, device="camera", width=640, heigth=480,
                 fx=1., fy=1., cx=.5, cy=.5,
                 k1=0., k2=0., k3=0., k4=0., k5=0., k6=0.,
                 p1=0., p2=0.):
        self.device = device
        self.width = float(width)
        self.heigth = float(heigth)
        self.fx = float(fx)
        self.fy = float(fy)
        self.cx = float(cx)
        self.cy = float(cy)
        self.k1 = float(k1)
        self.k2 = float(k2)
        self.k3 = float(k3)
        self.k4 = float(k4)
        self.k5 = float(k5)
        self.k6 = float(k6)
        self.p1 = float(p1)
        self.p2 = float(p2)

    def __repr__(self):
        return "<Camera Calibration: device = {0}, resolution={1}x{2}>".format(
               self.device, self.width, self.heigth)

    def __str__(self):
        return str(self.to_csv())

    def to_csv(self, path=None):
        """
        Save the calibration to a CSV file.
        """
        out = pd.DataFrame()
        out.loc[0, "device"] = self.device
        out.loc[0, "width"] = self.width
        out.loc[0, "heigth"] = self.heigth
        out.loc[0, "fx"] = self.fx
        out.loc[0, "fy"] = self.fy
        out.loc[0, "cx"] = self.cx
        out.loc[0, "cy"] = self.cy
        out.loc[0, "k1"] = self.k1
        out.loc[0, "k2"] = self.k2
        out.loc[0, "k3"] = self.k3
        out.loc[0, "k4"] = self.k4
        out.loc[0, "k5"] = self.k5
        out.loc[0, "k6"] = self.k6
        out.loc[0, "p1"] = self.p1
        out.loc[0, "p2"] = self.p2
        out = out.transpose()[0]
        if path is not None:
            out.to_csv(path)
        return out

    def get_camera_matrix(self):
        """
        Returns pinhole camera matrix.
        """
        out = np.zeros((3, 3))
        out[0, 0] = self.fx
        out[1, 1] = self.fy
        out[0, 2] = self.cx
        out[1, 2] = self.cy
        out[2, 2] = 1.0
        return out

    def get_distortion_coefficients(self):
        """
        Returns the distortion coefficients.
        """
        out = np.zeros(8)
        out[0] = self.k1
        out[1] = self.k2
        out[2] = self.p1
        out[3] = self.p2
        out[4] = self.k3
        out[5] = self.k4
        out[6] = self.k5
        out[7] = self.k6
        return out

    def change_resolution(self, width, heigth):
        """
        Updates the camera matrix coefficients width for a new resolution.
        TODO : Need to be checked

        nw, nh = width, heigth
        out = copy.copy(self)
        kx = nw / out.width
        ky = nh / out.heigth
        out.fx *= kx
        out.fy *= ky
        out.cx *= kx
        out.cy *= ky
        return out
        """
        pass

    def undisor_image(self, frame=None, plot=True):
        """
        undistore image

        inputs:
            img : (2x2 array like) image to undistore
            plot : (boolean) to plot raw image and undistor image
        """
        img_undist = cv2.undistort(
            frame,
            self.get_camera_matrix(),
            self.get_distortion_coefficients())
        if plot:
            plt.figure()
            ax1 = plt.subplot(311)
            plt.imshow(frame)
            plt.title("Raw image")
            plt.axis("off")
            plt.subplot(312, sharex=ax1, sharey=ax1)
            plt.imshow(img_undist)
            plt.title("Corrected image")
            plt.axis("off")
            plt.subplot(313, sharex=ax1, sharey=ax1)
            plt.imshow(np.abs(frame[:, :, 0] - img_undist[:, :, 0]))
            plt.title("Abs( Raw image - Corrected image)")
            plt.axis("off")
            plt.show()
        return img_undist

    def get_camera_matrix_OPENGL(self):
        """
        Convert camera matrix to OPENGL format
        ref: https://blog.noctua-software.com/
            opencv-opengl-projection-matrix.html
        """
        mv = np.zeros((4, 4))

        width = self.width
        height = self.heigth
        fx = self.fx
        fy = self.fy
        cx = self.cx
        cy = self.cy
        znear = 0.01
        zfar = 1.

        mv[0][0] = 2.0 * fx / width
        mv[0][1] = 0.0
        mv[0][2] = 0.0
        mv[0][3] = 0.0

        mv[1][0] = 0.0
        mv[1][1] = -2.0 * fy / height
        mv[1][2] = 0.0
        mv[1][3] = 0.0

        mv[2][0] = 1.0 - 2.0 * cx / width
        mv[2][1] = 2.0 * cy / height - 1.0
        mv[2][2] = (zfar + znear) / (znear - zfar)
        mv[2][3] = -1.0

        mv[3][0] = 0.0
        mv[3][1] = 0.0
        mv[3][2] = 2.0 * zfar * znear / (znear - zfar)
        mv[3][3] = 0.0
        return mv
