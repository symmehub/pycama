# Filename: <camera.py>
# Copyright (C) <2021> Authors: <Pierre Vacher, Ludovic Charleux, Emile Roux, Christian Elmo Kulanesan>
#
# This program is free software: you can redistribute it and / or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-

from sys import platform as _platform, stderr
import platform

import cv2
import numpy as np

if platform.processor() == "aarch64":
    try:
        from harvesters import __version__ as harvester_version
        from harvesters.core import Harvester
    except ModuleNotFoundError:
        print("Harvesters module not implemented for ARM architectures", file=stderr)
else:
    from harvesters import __version__ as harvester_version
    from harvesters.core import Harvester


from pycama import utils
from sys import platform as _platform
# ####################################
# Camera Class definition
# ####################################


class Camera():
    """
    Generic class to manage camera
    """

    def __init__(self):
        self.frame_h = 350
        self.frame_w = 700
        self.exposure = 0
        self.img_counter = 0

    def GUIable_Parameters(self):
        # GUIable Parameter list
        ParList = []
        tunePar = {}
        tunePar["label"] = "Exposure (ms)"
        tunePar["minValue"] = 0.001
        tunePar["maxValue"] = 250.
        tunePar["value"] = 10.
        tunePar["connect"] = self.set_exposure
        ParList.append(tunePar)
        tunePar = {}
        tunePar["label"] = "fps (/s)"
        tunePar["minValue"] = 1
        tunePar["maxValue"] = 60.
        tunePar["value"] = 10.
        tunePar["connect"] = self.set_fps
        ParList.append(tunePar)
        return ParList

    def get_frame(self):
        """
        dummy ge_frame methode for testing, retrurn a random image
        """
        frame = (0.5 - np.random.rand(self.frame_w, self.frame_h)) * .5
        # frame = (frame + self.img)
        frame = (np.abs(frame) / frame.max()) * 255

        return frame.astype(np.uint8)

    def get_and_save_frame(self, out_name='img', out_format='tif'):
        frame = self.get_frame()
        self.save_frame(frame, out_name, out_format)

    def save_frame(self, frame, out_name='img', out_format='tif'):
        img_name = "{0}_{1:04d}.{2}".format(
            out_name, self.img_counter, out_format)
        utils.save_frame(frame, img_name)
        print("{} written!".format(img_name))
        self.img_counter += 1

    def stream(self, out_name='img', out_format='tif'):
        """
        Simple methode to stream the video flux from a camera
        Opencv is use for display

        Use :
            - press Space : save image
            - press ESC : exit stream

        Input :
            - out_name : name or path to save capture images

        """
        img_counter = 0
        print("**** Hit SPACE to capture the frame    ****")
        print("**** Hit ESC to quit                   ****")
        while True:
            frame = self.get_frame()
            k = cv2.waitKey(1)
            if k % 256 == 32:
                # SPACE pressed
                self.save_frame(frame, out_name, out_format)
            if k % 256 == 27:
                # ESC pressed
                print("Escape hit, closing...")
                cv2.destroyWindow("frame")
                break
            # Make the view window resizable
            cv2.namedWindow('frame', cv2.WINDOW_NORMAL)
            cv2.imshow('frame', self.convert_frame(frame.copy()))

    def convert_frame(self, frame):
        """
        To convert frame to Unit8 if needed
        and add some text on frame befor to stream the flux.
        """
        return frame

    def release(self):
        pass

    def set_frame_size(self, width=1080, height=720):
        self.frame_w = width
        self.frame_h = height

    def set_exposure(self, exposure=0):
        pass

    def set_fps(self, fps=0):
        pass


class CameraOpencv(Camera):
    """
    Class to manage camera with opencv
    """

    # def GUIable_Parameters(self):
    #     """
    #     add extra guiable paramters on top of common ones
    #     """
    #     ParList = super().GUIable_Parameters()
    #     tunePar = {}
    #     tunePar["label"] = "focus (/s)"
    #     tunePar["minValue"] = 0
    #     tunePar["maxValue"] = 100.
    #     tunePar["value"] = 0.
    #     tunePar["connect"] = self.set_focus
    #     ParList.append(tunePar)
    #     return ParList

    def __init__(self, id_cam=0, exposure=100, focus=0, backen=cv2.CAP_DSHOW):
        super().__init__()
        if not backen is None:
            # , cv2.CAP_MSMF CAP_DSHOW
            self.cap = cv2.VideoCapture(id_cam, backen)
            print("BackendName=", self.cap.getBackendName())
        else:
            self.cap = cv2.VideoCapture(id_cam)
            print("No Backen specified")
        # Prop settings
        # Img size
        self.set_frame_size()
        # exposure time
        self.set_exposure(exposure)
        # focus
        self.set_focus(focus)
        # fps
        self.set_fps(20)

        self.cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))

    def _set_and_retrive_val(self, param_name=None,
                             val=0, param_description=""):
        # set the value
        self.cap.set(param_name, val)
        # retrive the value
        ret_val = self.cap.get(param_name)
        print('{0} set to : {1} (wanted {2})'.format(
            param_description, ret_val, val))

        return ret_val

    def set_frame_size(self, width=1920, height=1080):
        self._set_and_retrive_val(
            cv2.CAP_PROP_FRAME_HEIGHT, height, "cv2.CAP_PROP_FRAME_HEIGHT")
        self._set_and_retrive_val(
            cv2.CAP_PROP_FRAME_WIDTH, width, "cv2.CAP_PROP_FRAME_WIDTH")
        print("Frame size : ({0},{1})".format(self.cap.get(
            cv2.CAP_PROP_FRAME_WIDTH),
            self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))

    def set_exposure(self, exposure=500):
        """
        Set exposure time in ms
        TODO : compatilbily with lunix
        """
        # Stop autofocus
        self.cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 0)
        # Set exposure
        if _platform == 'linux':
            self._set_and_retrive_val(
                cv2.CAP_PROP_EXPOSURE, exposure, "cv2.CAP_PROP_EXPOSURE")
        else:
            # compute exposuer code (only foe windows)
            exposure_code = np.ceil(np.log2(exposure / 1000))
            self._set_and_retrive_val(
                cv2.CAP_PROP_EXPOSURE, exposure_code, "cv2.CAP_PROP_EXPOSURE")

    def set_fps(self, fps=10):
        self._set_and_retrive_val(
            cv2.CAP_PROP_FPS, fps, "cv2.CAP_PROP_FPS")

    def set_focus(self, focus=0):
        self._set_and_retrive_val(
            cv2.CAP_PROP_FOCUS, focus, "cv2.CAP_PROP_FOCUS")

    def get_frame(self):
        ret, frame = self.cap.read()
        return frame

    def release(self):
        self.cap.release()


if platform.processor() != "aarch64":
    class CameraGenICam(Camera):
        """
        Class to manage camera with Harvester and GeniCam standart
        """

        # def GUIable_Parameters(self):
        #     """
        #     add extra guiable paramters on top of common ones
        #     """
        #     ParList = super().GUIable_Parameters()
        #     tunePar = {}
        #     tunePar["label"] = "Binning"
        #     tunePar["minValue"] = 0
        #     tunePar["maxValue"] = 2
        #     tunePar["value"] = 0
        #     tunePar["connect"] = self.set_binning
        #     ParList.append(tunePar)
        #     return ParList

        def __init__(self, path2cti=None,
                     binningV=1,
                     binningH=1,
                     exposureTime=5000,
                     fps=10):
            super().__init__()

            if int(harvester_version.split('.')[0]) == 0:
                self.h = Harvester()
                # GenICam file
                self.h.add_cti_file(path2cti)

                self.h.update_device_info_list()
                self.ia = self.h.create_image_acquirer(0)
                dir(self.ia.device.node_map)

                # Settings of the camera
                self.ia.device.node_map.PixelFormat.value = 'Mono8'

                # Freez Gain setting to Zero
                self.ia.device.node_map.Gain.value = 0
                self.ia.device.node_map.GainAuto.value = 'Off'
                self.ia.device.node_map.GainAutoLevel.value = 0

                self.set_fps(fps=fps)
                self.set_exposure(exposure=exposureTime)
                self.set_binning(binning=binningH)

                self.ia.start_image_acquisition()
            else:
                self.h = Harvester()
                # GenICam file
                self.h.add_file(path2cti)

                self.h.update()
                self.ia = self.h.create_image_acquirer(0)
                dir(self.ia.remote_device.node_map)

                # Settings of the camera
                self.ia.remote_device.node_map.Width.value = self.ia.remote_device.node_map.WidthMax.value
                self.ia.remote_device.node_map.Height.value = self.ia.remote_device.node_map.HeightMax.value
                self.ia.remote_device.node_map.PixelFormat.value = 'Mono8'
                # Freez Gain setting to Zero
                self.ia.remote_device.node_map.Gain.value = 0
                self.ia.remote_device.node_map.GainAuto.value = 'Off'
                self.ia.remote_device.node_map.GainAutoLevel.value = 0

                self.set_fps(fps=fps)
                self.set_exposure(exposure=exposureTime)
                self.set_binning(binning=binningH)

                self.ia.start_acquisition(True)


        def set_fps(self, fps=10):
            if int(harvester_version.split('.')[0]) == 0:
                self.ia.device.node_map.AcquisitionFrameRate.value = fps
            else:
                self.ia.remote_device.node_map.AcquisitionFrameRate.value = fps

        def set_exposure(self, exposure=5000):
            if int(harvester_version.split('.')[0]) == 0:
                self.ia.device.node_map.ExposureTime.value = exposure
            else:
                self.ia.remote_device.node_map.ExposureTime.value = exposure

        def set_binning(self, binning=0):
            if int(harvester_version.split('.')[0]) == 0:
                self.ia.device.node_map.BinningHorizontal.value = binning
                self.ia.device.node_map.BinningVertical.value = binning
            else:
                self.ia.remote_device.node_map.BinningHorizontal.value = binning
                self.ia.remote_device.node_map.BinningVertical.value = binning

        def get_frame(self):
            with self.ia.fetch_buffer() as buffer:
                component = buffer.payload.components[0]
                x2d = component.data.reshape(component.height, component.width)
                frame = x2d.copy()
                return frame

        def release(self):
            self.ia.stop_image_acquisition()
            if int(harvester_version.split('.')[0]) > 0:
                self.ia.destroy()
            self.h.reset()
            
    class CameraGenICamFlir(CameraGenICam):
        """
        Class to manage camera with Harvester and GeniCam standart
        specific class to manage Flir camere Mono16
        
        Instal Spinnaker to get the cti file (on windows)
        url : https://www.flir.com/products/spinnaker-sdk/
        
        """

        # def GUIable_Parameters(self):
        #     """
        #     add extra guiable paramters on top of common ones
        #     """
        #     ParList = super().GUIable_Parameters()
        #     tunePar = {}
        #     tunePar["label"] = "Binning"
        #     tunePar["minValue"] = 0
        #     tunePar["maxValue"] = 2
        #     tunePar["value"] = 0
        #     tunePar["connect"] = self.set_binning
        #     ParList.append(tunePar)
        #     return ParList

        def __init__(self, path2cti=None):

            self.h = Harvester()
            # GenICam file
            self.h.add_file(path2cti)

            self.h.update()
            self.ia = self.h.create_image_acquirer(0)
            dir(self.ia.remote_device.node_map)
            # Settings of the camera
            self.ia.remote_device.node_map.Width.value = self.ia.remote_device.node_map.WidthMax.value
            self.ia.remote_device.node_map.Height.value = self.ia.remote_device.node_map.HeightMax.value
            self.ia.remote_device.node_map.PixelFormat.value = 'Mono16'
            self.ia.remote_device.node_map.IRFormat.value = 'TemperatureLinear10mK'
            self.ia.start_acquisition(True)


        def get_frame(self):
            """
            Convert frame forme uint16 to K isung the 10mK rule
            """
            frame = super().get_frame()            
            frame = frame*0.01 - 273.15 # from mono16 to degres
            frame = frame[:-3, :] # remove last lines 
            return frame
        
        def convert_frame(self, frame):
            """
            Convert frame from mono16 to uint8 for display in streaming mode.
            and add meta data on top of image
            Parameters
            ----------
            frame : frame MONO16

            Returns
            -------
                frame Uint8.

            """
            Tmax = frame.max()
            Tmin = frame.min()
            frame = frame-frame.min()
    
            frame = 255*frame/frame.max()
            frame = frame.astype(np.uint8)
            frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2RGB)
    
            font = cv2.FONT_HERSHEY_SIMPLEX
            bottomLeftCornerOfText = (10, 30)
            fontScale = 0.5
            fontColor = (0, 255, 0)
            lineType = 2
    
            cv2.putText(frame, 'TC =[{0:0.2f}, {1:0.2f}]'.format(Tmin, Tmax),
                        bottomLeftCornerOfText,
                        font,
                        fontScale,
                        fontColor,
                        lineType)
    
            return frame.astype(np.uint8)
        
        def release(self):
            print('Set initial settings for ResercheIR')
            self.ia.remote_device.node_map.IRFormat.value = 'Radiometric'
            super().release()   
            



if _platform == "linux" or _platform == "linux2":
    class CameraAravis(Camera):
        def __init__(self,
                     binningV=1,
                     binningH=1,
                     exposureTime=5000,
                     fps=10):
            super().__init__()
            import gi
            # Importing gi makes python enabled to import aravis
            gi.require_version('Aravis', '0.8')
            from aravis import Camera as arvCamera
            self.cam = arvCamera()

            self.set_fps(fps=fps)
            self.set_exposure(exposure=exposureTime)
            self.set_binning(binning=binningH)

            self.cam.start_acquisition_continuous()

        def get_frame(self):
            frame = self.cam.pop_frame()
            return frame

        def set_fps(self, fps=10):
            self.cam.set_frame_rate(fps)
            print("current fps=\t", self.cam.get_frame_rate())

        def set_exposure(self, exposure=5000):
            self.cam.set_exposure_time(exposure)
            print("current exposure time=\t", self.cam.get_exposure_time())

        def set_binning(self, binning=0):
            self.cam.set_binning(dx=binning, dy=binning)

        def release(self):
            self.cam.stop_acquisition()
            self.cam.shutdown()

        def device_list_info(self):
            self.ids = aravis.get_device_ids()


class FlirCam(Camera):
    """ 
    Deprecated class : use CameraGenICamFlir instead
    """
    def __init__(self):
        print('Deprecated class : use CameraGenICamFlir instead')
        super().__init__()
        import simple_pyspin
        self.cap = simple_pyspin.Camera()  # Acquire Camera
        self.cap.init()  # Initialize camera
        # Set to
        self.cap.IRFormat = 'TemperatureLinear10mK'
        self.cap.AcquisitionMode = 'Continuous'  # SingleFrame Continuous
        #self.cap.StreamBufferHandlingMode = "Newest First"
        self.cap.start()  # Start recording

    def get_frame(self):
        if self.cap.running == False:
            self.cap.start()
        im = self.cap.get_image()
        frame = im.GetNDArray()*0.01 - 273.15
        timeStamp = im.GetTimeStamp()
        print(timeStamp)
        #frame = self.cap.get_array()*0.01 - 273.15
        self.cap.stop()
        return frame[:-10, :]

    def save_frame(self, frame, out_name='img', out_format='tif'):
        img_name = "{0}_{1:04d}.{2}".format(
            out_name, self.img_counter, out_format)
        utils.save_frame(self.convert_frame(frame), img_name)
        np.save(img_name[:-4]+'_T', frame)
        print("{} written!".format(img_name))
        self.img_counter += 1

    def convert_frame(self, frame):

        Tmax = frame.max()
        Tmin = frame.min()
        frame = frame-frame.min()

        frame = 255*frame/frame.max()
        frame = frame.astype(np.uint8)
        frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2RGB)

        font = cv2.FONT_HERSHEY_SIMPLEX
        bottomLeftCornerOfText = (10, 30)
        fontScale = 0.5
        fontColor = (0, 255, 0)
        lineType = 2

        cv2.putText(frame, 'TC =[{0:0.2f}, {1:0.2f}]'.format(Tmin, Tmax),
                    bottomLeftCornerOfText,
                    font,
                    fontScale,
                    fontColor,
                    lineType)

        return frame.astype(np.uint8)

    def release(self):
        self.cap.stop()
        self.cap.close()


if __name__ == '__main__':
    # Create cam
    import sys
    if sys.argv[1] is not None:
        if sys.argv[1] == "opencv":
            cam = CameraOpencv(1)
            cam.stream()

        if sys.argv[1] == "genicam":
            if platform.processor() != 'aarch64':
                path2cti = r'/opt/mvIMPACT_Acquire/lib/x86_64/mvGenTLProducer.cti'
                cam = CameraGenICam(path2cti=path2cti,
                                    binningV=1,
                                    binningH=1,
                                    exposureTime=100000,
                                    fps=10)
            else:
                print("Can't use genicam for ARM architectures", file=stderr)
