# Filename: <camera_GUI.py>
# Copyright (C) <2021> Authors: <Pierre Vacher, Ludovic Charleux, Emile Roux, Christian Elmo Kulanesan>
# 
# This program is free software: you can redistribute it and / or 
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 2 of the License. 
# 
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of  
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
# GNU General Public License for more details.  
# 
# You should have received a copy of the GNU General Public License  
# along with this program.  If not, see <https://www.gnu.org/licenses/>. 
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 14 14:30:34 2020

@author: rouxemi
"""
import sys
from PyQt5 import QtWidgets, QtCore
from pycama import camera

try:
    import animated_oscillators as animos
except ModuleNotFoundError:
    print("Module animated_oscillators not found", file=sys.stderr)


##############################################################################
# MANAGER MAIN WINDOW
##############################################################################


class CameraManager(QtWidgets.QMainWindow):
    """
    Oscillator manager class.
    """

    def __init__(self, *args, **kwargs):
        super(CameraManager, self).__init__(*args, **kwargs)
        self.cam = None
        self.init_ui()

    def init_ui(self):
        # MAIN LAYOUT (obliger de la definir pour personnaliser l'interface)
        self.statusBar().showMessage('Make movies with poneys !')
        # disposition verticale des widgets
        self.mainLayout = QtWidgets.QVBoxLayout()
        # creation d'un widget par appel de QWidget
        self.centralWidget = QtWidgets.QWidget()
        # on lui applique la disposition def pour les widgets
        self.centralWidget.setLayout(self.mainLayout)
        self.setCentralWidget(self.centralWidget)  # on l'ajoute a la fenetre

        # Camera choice LAYOUT
        self.camChoiceLayout = QtWidgets.QHBoxLayout()
        self.camChoiceLayout.addWidget(QtWidgets.QLabel("Camera choice"))
        # creation du widget CamChoiceWidget
        self.camChoiceWidget = QtWidgets.QWidget()
        # on lui applique un certain affichage
        self.camChoiceWidget.setLayout(self.camChoiceLayout)
        # on l'ajoute a la presentation de la fenetre principale de l'API
        self.mainLayout.addWidget(self.camChoiceWidget)
        # Opencv buton
        self.buttonOpencv = QtWidgets.QPushButton('Opencv', self)
        self.buttonOpencv.clicked.connect(self.on_click_Opencv)
        self.camChoiceLayout.addWidget(self.buttonOpencv)

        # GenICam buton
        self.buttonGenICam = QtWidgets.QPushButton('GenICam', self)
        self.buttonGenICam.clicked.connect(self.on_click_GenICam)
        self.camChoiceLayout.addWidget(self.buttonGenICam)

        # Création de widget CameraParamterWidget et ajout au main layout
        self.cameraWidget = CameraParamterWidget(self.cam)
        self.mainLayout.addWidget(self.cameraWidget)

        # Capture LAYOUT
        # def de la mise en page du futur widget pour l'excitation
        self.captureLayout = QtWidgets.QHBoxLayout()
        self.captureLayout.addWidget(QtWidgets.QLabel("Image Capture"))
        # creation du widget captureWidget
        self.captureWidget = QtWidgets.QWidget()
        # on lui applique un certain affichage
        self.captureWidget.setLayout(self.captureLayout)
        # on l'ajoute a la presentation de la fenetre principale de l'API
        self.mainLayout.addWidget(self.captureWidget)

        # Stream buton
        self.buttonStream = QtWidgets.QPushButton('Stream', self)
        self.buttonStream.clicked.connect(self.on_click_stream)
        self.captureLayout.addWidget(self.buttonStream)

        # Capture img buton
        self.buttonSave = QtWidgets.QPushButton('Capture', self)
        self.buttonSave.clicked.connect(self.on_click_save)
        self.captureLayout.addWidget(self.buttonSave)

    def on_click_Opencv(self):
        if self.cam is not None:
            self.cam.release()
        self.cam = self.cam = camera.CameraOpencv(id_cam=1,
                                                  exposure=-5,
                                                  focus=0)
        self.cameraWidget.update(
            camera=self.cam, decription="Camera Settings: OpenCV")

    def on_click_GenICam(self):
        if self.cam is not None:
            self.cam.release()
        self.cam = self.cam = camera.CameraGenICam(path2cti="xxx")
        self.cameraWidget.update(
            camera=self.cam, decription="Camera Settings: GenICam")

    def on_click_stream(self):
        self.cam.stream()

    def on_click_save(self):
        self.cam.get_and_save_frame()

    def update(self):
        QtCore.QTimer.singleShot(10, self.update)


##############################################################################
# WIDGET CameraParamterWidget
##############################################################################


class CameraParamterWidget(QtWidgets.QGroupBox):
    def __init__(self, camera=None, parent=None, *args, **kwargs):
        super(CameraParamterWidget, self).__init__(
            parent=parent, *args, **kwargs)
        self.cam = camera
        self.init_ui()

    def update(self, camera=None, decription="Camera Settings"):
        self.cam = camera
        self.labelWidget.setText(decription)
        self.addSliders()

    def init_ui(self):
        self.mainLayout = QtWidgets.QVBoxLayout()
        self.labelWidget = QtWidgets.QLabel("Camera Settings",)
        self.mainLayout.addWidget(self.labelWidget)
        self.setLayout(self.mainLayout)

        self.scrollLayout = QtWidgets.QVBoxLayout()
        self.scrollWidget = QtWidgets.QWidget()  # creation d'un Widget
        # application de la disposition def pour les widgets
        self.scrollWidget.setLayout(self.scrollLayout)
        # def d'une barre de defilement dans le menu
        self.scrollArea = QtWidgets.QScrollArea()
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setWidget(self.scrollWidget)
        # ajout du widget dans la fenetre principale liee a la barre de scroll
        self.mainLayout.addWidget(self.scrollArea)

    def addSliders(self):
        # Clear the scrollLayout befor adding new widgets
        for i in reversed(range(self.scrollLayout.count())):
            self.scrollLayout.itemAt(i).widget().setParent(None)

        # get the list of guiable parameter form camera
        ParList = self.cam.GUIable_Parameters()
        # Bluid sliders from list
        for tuneP in ParList:
            self.parSlider = animos.utils.MySlider(label=tuneP["label"],
                                                   minValue=tuneP["minValue"],
                                                   maxValue=tuneP["maxValue"],
                                                   value=tuneP["value"])
            self.parSlider.connect(tuneP["connect"])
            self.scrollLayout.addWidget(self.parSlider)


##############################################################################
# APP EXECUTION
##############################################################################


def main():
    app = QtWidgets.QApplication(sys.argv)
    app.setApplicationName("Camera Manager")
    main = CameraManager()
    main.show()
    # ex=CameraParamterWidget()
    # ex.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
