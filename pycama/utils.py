# Filename: <utils.py>
# Copyright (C) <2021> Authors: <Pierre Vacher, Ludovic Charleux, Emile Roux, Christian Elmo Kulanesan>
#
# This program is free software: you can redistribute it and / or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# #############################################################################
# PYCAMA/CALIBRATION
# #############################################################################


import matplotlib.pyplot as plt
import numpy as np
from skimage.transform import resize
import skimage.io as io
from PIL import Image


# save imgage whitout compression
def save_frame(frame,filename):
    """
    Save image whitout compression, works only for tif format

    Parameters
    ----------
    frame : array
        imafe data
    filename : string
        name of yhe saved images

    Returns
    -------
    None.

    """
    im=Image.fromarray(frame)
    if filename[-3:] != 'tif':
        print('save image format not supported')
    im.save(filename, compression='none')

# ChessBoard generation for camera calibration
def GenChessBoard(nx=19, ny=12,
                  square_size=75,
                  img_resolution=(1920, 1200),
                  plot=False,
                  save=True):
    """
    Fonction to create chessbord image for camera calibration

    Inputs:
        nx: (int) number of square in x dierction
        ny: (int) number of square in x dierction
        square_size: (int) size of square in pix
        img_resolution: (int,int) resolution of the output image
        plot: plot the results

    """
    img_back = np.ones((img_resolution[1], img_resolution[0]))

    if (ny % 2 == 0):
        ny = ny + 1

    img = np.zeros((int(ny), int(nx)))

    img_f = np.ravel(img)
    img_f[::2] = 1.

    img_r = resize(img, (ny * square_size, nx * square_size), order=0)
    cx, cy = img_r.shape
    bx, by = img_back.shape
    off_x = int((bx - cx) / 2)
    off_y = int((by - cy) / 2)

    img_back[off_x:cx + off_x, off_y:cy + off_y] = img_r

    img_back = (255 * img_back).astype(np.uint8)

    if plot:
        plt.figure()
        plt.imshow(img_back, cmap='gray')

    if save:
        name = "chessBoard_{0:d}_{1:d}.png".format(int(nx) - 1, int(ny) - 1)
        io.imsave(name, img_back)
        print('Chess board save as ' + name)

    return img_back


if __name__ == '__main__':
    # Sreen resolution
    screen_resolution = (1920, 1200)

    # chessboard size
    nx = 19
    ny = 12

    # square size in pix
    square_size = 75
    img_back = GenChessBoard(nx=nx, ny=ny,
                             square_size=square_size,
                             img_resolution=screen_resolution,
                             plot=True)
