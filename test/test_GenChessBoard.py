# Filename: <test_GenChessBoard.py>
# Copyright (C) <2021> Authors: <Pierre Vacher, Ludovic Charleux, Emile Roux, Christian Elmo Kulanesan>
#
# This program is free software: you can redistribute it and / or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 14:07:48 2019

@author: rouxemi

"""

import numpy as np

import os
import pycama as pca
import skimage.io as io

# Solution
module_path = os.path.dirname(pca.__file__)
chessBord_sol = io.imread(module_path + '/../test/data/chessBoard_18_12.png')


# Sreen resolution
screen_resolution = (1920, 1200)

# chessboard size
nx = 19
ny = 12
# square size in pix
square_size = 75

chessBord = pca.utils.GenChessBoard(nx=nx,
                                    ny=ny,
                                    square_size=square_size,
                                    img_resolution=screen_resolution,
                                    plot=False,
                                    save=False)


def test_img():
    assert (((chessBord_sol - chessBord)**2).sum()**.5 < 0.01)


if __name__ == '__main__':
    print(test_img())
