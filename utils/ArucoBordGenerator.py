# Filename: <ArucoBordGenerator.py>
# Copyright (C) <2021> Authors: <Pierre Vacher, Ludovic Charleux, Emile Roux, Christian Elmo Kulanesan>
# 
# This program is free software: you can redistribute it and / or 
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 2 of the License. 
# 
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of  
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
# GNU General Public License for more details.  
# 
# You should have received a copy of the GNU General Public License  
# along with this program.  If not, see <https://www.gnu.org/licenses/>. 
# -*- coding: utf-8 -*-
"""
Created on Thu May  2 13:49:46 2019

@author: rouxemi
"""


from cv2 import aruco
import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import cm
import copy
import skimage.io as io
from skimage.transform import resize
from skimage import filters


# =============================================================================
# ChessBoard generation for camera calibration
# =============================================================================
# Use Irfan : https://www.irfanview.com/
#
# View/Fullscreen options: desactive them all
# =============================================================================
# INPUTS
# =============================================================================
# Sreen resolution
screen_resolution = (1920, 1200)

# chessboard size
nx = 19
ny = 12

# square size in pix
Square_size = 80

aruco_dict=aruco.Dictionary_get(aruco.DICT_6X6_250)
# =============================================================================
#
# =============================================================================

## PLOT CHARUCO BOARD
workdir = "./workdir/"
board = aruco.CharucoBoard_create(18, 12, 1, .75, aruco_dict)
img_back = board.draw((2560, 1440))
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
plt.imshow(img_back, cmap = mpl.cm.gray, interpolation = "nearest")
ax.axis("off")
plt.show()



"""
img_back = np.ones((screen_resolution[1], screen_resolution[0]))

if (ny % 2 == 0):
    ny = ny + 1

img = np.zeros((int(ny), int(nx)))

img_f = np.ravel(img)
img_f[::2] = 1.

img_r = resize(img, (ny * Square_size, nx * Square_size), order=0)
cx, cy = img_r.shape
bx, by = img_back.shape

off_x = int((bx - cx) / 2)
off_y = int((by - cy) / 2)


img_back[off_x:cx + off_x, off_y:cy + off_y] = img_r

plt.figure()
plt.imshow(img_r)
plt.show()

plt.figure()
plt.imshow(img_back)
"""

io.imsave("AucoChess_{0:d}_{1:d}.png".format(int(nx) - 1, int(ny) - 1), img_back)
