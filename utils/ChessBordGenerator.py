# Filename: <ChessBordGenerator.py>
# Copyright (C) <2021> Authors: <Pierre Vacher, Ludovic Charleux, Emile Roux, Christian Elmo Kulanesan>
# 
# This program is free software: you can redistribute it and / or 
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 2 of the License. 
# 
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of  
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
# GNU General Public License for more details.  
# 
# You should have received a copy of the GNU General Public License  
# along with this program.  If not, see <https://www.gnu.org/licenses/>. 
# -*- coding: utf-8 -*-
"""
Created on Thu May  2 13:49:46 2019

@author: rouxemi
"""


import copy

import cv2
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import skimage.io as io
from cv2 import aruco
from matplotlib import cm
from skimage import filters
from skimage.transform import resize

# =============================================================================
# ChessBoard generation for camera calibration
# =============================================================================
# Use Irfan : https://www.irfanview.com/
#
# View/Fullscreen options: desactive them all
# =============================================================================
# INPUTS
# =============================================================================
# Sreen resolution
screen_resolution = (4000, 4000)

# chessboard size
nx = 19
ny = 12

# square size in pix
Square_size = 200


# =============================================================================
#
# =============================================================================
img_back = np.ones((screen_resolution[1], screen_resolution[0]))

if (ny % 2 == 0):
    ny = ny + 1

img = np.zeros((int(ny), int(nx)))

img_f = np.ravel(img)
img_f[::2] = 1.

img_r = resize(img, (ny * Square_size, nx * Square_size), order=0)
cx, cy = img_r.shape
bx, by = img_back.shape

off_x = int((bx - cx) / 2)
off_y = int((by - cy) / 2)


img_back[off_x:cx + off_x, off_y:cy + off_y] = img_r

plt.figure()
plt.imshow(img_r)
plt.show()

plt.figure()
plt.imshow(img_back)


io.imsave("chess_{0:d}_{1:d}.tif".format(int(nx) - 1, int(ny) - 1), img_back)
